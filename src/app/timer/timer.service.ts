import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})

export class TimerService {
    months = [
        'Jan', 'Feb', 'Mar', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec',
    ];
    cDateMillisecs: any;
    tDateMillisecs: any;
    year: number = 2022;
    month: number = 9;
    currentDate: any;
    targetDate: any;
    difference: any;
    seconds: any;
    minutes: any;
    hours: any;
    days: any;
    day: number = 31;
    display: any;
    interval: any;

    startTimer() {
        this.interval = setInterval(() => {
            this.currentDate = new Date();
            this.targetDate = new Date(this.year, this.month, this.day);
    
            this.display = this.transform();
        }, 1000);
    }

    pauseTimer() {
        clearInterval(this.interval);
    }

    fetchDisplayTargetMonth() {
        return this.months[this.month];
    }

    fetchDisplayTargetDay() {
        return this.day;
    }

    fetchDisplayTargetYear() {
        return this.year;
    }

    fetchDisplay() {
        return this.display;
    }

    transform(): {[key: string]: number} {

        this.cDateMillisecs = this.currentDate.getTime();
        this.tDateMillisecs = this.targetDate.getTime();
        this.difference = this.tDateMillisecs - this.cDateMillisecs;
        this.seconds = Math.floor(this.difference / 1000);
        this.minutes = Math.floor(this.seconds / 60);
        this.hours = Math.floor(this.minutes / 60);
        this.days = Math.floor(this.hours / 24);

        this.hours %= 24;
        this.minutes %= 60;
        this.seconds %= 60;
        this.hours = this.hours < 10 ? '0' + this.hours : this.hours;
        this.minutes = this.minutes < 10 ? '0' + this.minutes : this.minutes;
        this.seconds = this.seconds < 10 ? '0' + this.seconds : this.seconds;
        return {
            "days": this.days,
            "minutes": this.minutes,
            "hours": this.hours,
            "seconds": this.seconds
        };

    }
}