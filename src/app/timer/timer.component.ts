import { AfterViewInit, Component } from '@angular/core';
import { TimerService } from "./timer.service";

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.sass'],
})
export class TimerComponent implements AfterViewInit {
  arr: any;

  constructor(private timerService: TimerService) {}

  ngAfterViewInit() {
    this.timerService.startTimer();
  }

  fetchDisplayTargetMonth() {
    return this.timerService.fetchDisplayTargetMonth();
  }

  fetchDisplayTargetDay() {
    return this.timerService.fetchDisplayTargetDay();
  }

  fetchDisplayTargetYear() {
    return this.timerService.fetchDisplayTargetYear();
  }

  fetchDisplay(time: string) {

    this.arr = this.timerService.fetchDisplay();

    if (this.arr !== undefined) {

      if (this.arr[time] !== undefined)
        return this.arr[time];
    }
    
    return null;
  }

}
