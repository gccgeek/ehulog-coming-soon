import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { TimerComponent } from './timer.component';

@NgModule({
  imports: [BrowserModule, FormsModule],
  declarations: [TimerComponent],
  bootstrap: [TimerComponent],
})
export class TimerModule { }