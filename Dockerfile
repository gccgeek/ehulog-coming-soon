FROM node:18-bullseye-slim

WORKDIR /usr/src/app

COPY ["package.json", "/usr/src/app/"]
COPY ["package-lock.json", "/usr/src/app/"]

RUN npm cache clean --force
RUN npm install -g @angular/cli
RUN npm ci --only=production

COPY . .

ENV PATH ./bin:$PATH

EXPOSE 4200

# standalone - dev
CMD ng serve --port 4200 --host 0.0.0.0 --poll 1

# production
# CMD ["ng","serve","--host", "0.0.0.0", "--disable-host-check"]
